import React, { Component } from 'react';
import axios from 'axios';

import './FullPost.css';

class FullPost extends Component {
    state = {
        loadedPost: null
    }
    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate() {
        this.loadData();
    }

    loadData() {
        if (this.props.match.params.id) {
            // conditional check is essential to avoid repeated loop render
            if (!this.state.loadedPost || (this.state.loadedPost && (this.state.loadedPost.id !== Number(this.props.match.params.id)))) {
                axios.get("http://jsonplaceholder.typicode.com/posts/" + this.props.match.params.id)
                    .then(response => {
                        this.setState({ loadedPost: response.data });
                    });
            }
        }
    }

    deletePostHandler = () => {
        axios.delete("http://jsonplaceholder.typicode.com/posts/" + this.props.match.params.id)
            .then(response => {
                console.log(response);
            });
    };

    render() {
        let post = <p style={{ textAlign: "center" }}>Please select a Post!</p>;
        /**
         * this alone may fail as we have a valid property, but not a valid post due to web call
         */
        if (this.props.match.params.id) {
            post = <p style={{ textAlign: "center" }}>Loading...!</p>;
        }
        // this ensures complete data is available before rendering
        if (this.state.loadedPost) {
            post = (
                <div className="FullPost">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="Edit">
                        <button className="Delete" onClick={this.deletePostHandler}>Delete</button>
                    </div>
                </div>

            );
        }
        return post;
    }
}

export default FullPost;