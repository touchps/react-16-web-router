import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import axios from 'axios';
import registerServiceWorker from './registerServiceWorker';

// global interceptor for all axios request calls
axios.interceptors.request.use(request => {
    console.log(request);

    // further stuff (headers/tokens) can be added
    // request is blocked if not returned
    return request;
}, error => {
    console.log(error);
    // this should be returned just like request to further forward the error
    return Promise.reject(error);
});

// gloabl interceptor for all axios responses
axios.interceptors.response.use(response => {
    console.log(response);
    return response;
}, error => {
    console.log(error);
    return Promise.reject(error);
});

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
